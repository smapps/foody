//
//  Dish.swift
//  Foody
//
//  Created by Sameh Mabrouk on 12/5/15.
//  Copyright © 2015 smapps. All rights reserved.
//
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import UIKit

class Dish: NSObject {

    // The dish id.
    var dish_id : String? = ""
    // The dish name.
    var dish_name : String? = ""
    // The dish image.
    var dish_imageUrl : String? = ""
    // The description of dish.
    var dish_description : String? = ""
    // The user rating of dish.
    var dish_userRating : String? = ""
    // The average rating of dish.
    var dish_averageRating : String? = ""
    // Is dish liked.
    var dish_isLiked : String?
    // The delivery date of dish.
    var dish_deliveryDate : String?

    var restaurant: Restaurant?

    init( dish_id : String?, dish_name : String?, dish_imageUrl : String?, dish_description : String?, dish_userRating : String?, dish_averageRating : String?, dish_isLiked : String?, dish_deliveryDate : String?, restaurant: Restaurant?) {

        self.dish_id = dish_id
        self.dish_name = dish_name
        self.dish_imageUrl = dish_imageUrl
        self.dish_description = dish_description
        self.dish_userRating = dish_userRating
        self.dish_averageRating = dish_averageRating
        self.dish_isLiked = dish_isLiked
        self.dish_deliveryDate = dish_deliveryDate
        self.restaurant = restaurant

        print("rest name..1", self.restaurant?.rest_name)


        super.init();
    }

}
