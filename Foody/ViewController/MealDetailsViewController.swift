//
//  MealDetailsViewController.swift
//  Foody
//
//  Created by Sameh Mabrouk on 11/21/15.
//  Copyright © 2015 smapps. All rights reserved.
//
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import UIKit
import Alamofire
import AlamofireImage

class MealDetailsViewController: UIViewController, FloatRatingViewDelegate {

    var dish:Dish?

    @IBOutlet var likeButton : UIButton!
    @IBOutlet var dislikeButton : UIButton!
    @IBOutlet var restImageView : UIImageView!
    @IBOutlet var dishImageView : UIImageView!
    @IBOutlet var dishOrderDateLabel : UILabel!
    @IBOutlet var dishDescTexView : UITextView!
    @IBOutlet var userRatingView: FloatRatingView!
    @IBOutlet var averageRatingView: FloatRatingView!

    override func viewWillAppear(animated: Bool) {

        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.tintColor = UIColor.blueColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.lightGrayColor()]

    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        let btnName = UIButton()
        btnName.setImage(UIImage(named: "back-Gray"), forState: .Normal)
        btnName.frame = CGRectMake(0, 0, 28, 28)
        btnName.addTarget(self, action: Selector("backAction"), forControlEvents: .TouchUpInside)

        //.... Set Right/Left Bar Button item
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.leftBarButtonItem = rightBarButton

        print("rest name", self.dish?.restaurant?.rest_name)

        //update UI.
        let URL = NSURL(string:self.dish!.dish_imageUrl!)!
        let restImageURL = NSURL(string: (self.dish!.restaurant?.rest_LogoPath)!)

//        let restImageURL = NSURL(string:(self.dish?.restaurant?.rest_LogoPath!)!)!

        dishImageView.af_setImageWithURL(URL)
        restImageView.af_setImageWithURL(restImageURL!)

        dishOrderDateLabel.text = self.dish?.dish_deliveryDate
        dishDescTexView.text = self.dish?.dish_description

        self.title = self.dish?.dish_name

        // Required user rating view params
        self.userRatingView.emptyImage = UIImage(named: "gray-star")
        self.userRatingView.fullImage = UIImage(named: "gold-star")
        // Optional params
        self.userRatingView.delegate = self
        self.userRatingView.contentMode = UIViewContentMode.ScaleAspectFit
        self.userRatingView.maxRating = 5
        self.userRatingView.minRating = 1
        self.userRatingView.rating = (dish!.dish_userRating! as NSString).floatValue
        self.userRatingView.editable = true
        self.userRatingView.halfRatings = false
        self.userRatingView.floatRatings = false

        // Required average rating view params
        self.averageRatingView.emptyImage = UIImage(named: "gray-star")
        self.averageRatingView.fullImage = UIImage(named: "orange-star")
        // Optional params
        self.averageRatingView.contentMode = UIViewContentMode.ScaleAspectFit
        self.averageRatingView.maxRating = 5
        self.averageRatingView.minRating = 1
        self.averageRatingView.rating = (dish!.dish_averageRating! as NSString).floatValue
        self.averageRatingView.editable = false
        self.averageRatingView.halfRatings = false
        self.averageRatingView.floatRatings = false

    }

    func backAction() {
        self.navigationController?.popViewControllerAnimated(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func ratingTypeChanged(sender: UISegmentedControl) {

    }

    // MARK: Actions

    @IBAction func showInterestAction(sender: UIButton!) {

        JTProgressHUD.show()


        print("button tag \(sender.tag)")

        var isLiked:String?

        if sender.tag == 100 {
            isLiked = "1"

            let button = sender as UIButton
            dispatch_async(dispatch_get_main_queue()) {
                self.likeButton.highlighted = button == self.likeButton
            }

            self.dislikeButton.highlighted = false


        }
        else {
            isLiked = "0"

            self.likeButton.highlighted = false

            let button = sender as UIButton
            dispatch_async(dispatch_get_main_queue()) {
                self.dislikeButton.highlighted = button == self.dislikeButton
            }

        }

        //call List Dishes API.
        let userDefaults = NSUserDefaults.standardUserDefaults()
        if let OAuthToken = userDefaults.valueForKey("AuthorizationToken") {
            // do something here when a OAuthToken exists
            Router.OAuthToken = OAuthToken as? String

        }
        else {
            // no OAuthToken exists
        }
        print("AuthorizationToken: \(Router.OAuthToken)")
        print("dish ID: \(dish?.dish_id)")
        print("User Rating : \(self.userRatingView.rating)")

        UIApplication.sharedApplication().networkActivityIndicatorVisible = true

        Alamofire.request(Router.showInterest((dish?.dish_id)!, isLiked!)).responseJSON { result in
            print(result)
            debugPrint(result)
            print("Parsed JSON: \(result.result.value)")
            print("JSON Error: \(result.result.error)")

            print("JSON Error2: \(result.result.value!["errors"])")
            print("JSON Error2: \(result.result.value!["data"])")

            let y: AnyObject = (result.result.value?["data"])!
            if let _ = y as? Int {
                print("Yes")


            }
            else{
                print("No")

                //display error alert.
                if let errorMsg: AnyObject = (result.result.value?["errors"])!{

                    let alert = UIAlertController(title: "Error", message:errorMsg as? String , preferredStyle: UIAlertControllerStyle.Alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                    alert.addAction(defaultAction)
                    self.presentViewController(alert, animated: true, completion: nil)
                }

            }

            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            JTProgressHUD.hide()

        }

    }

    // MARK: FloatRatingViewDelegate

    func floatRatingView(ratingView: FloatRatingView, isUpdating rating:Float) {
        print("isUpdating")
    }

    func floatRatingView(ratingView: FloatRatingView, didUpdate rating: Float) {
        print("didUpdate")
        //call List Dishes API.
        let userDefaults = NSUserDefaults.standardUserDefaults()
        if let OAuthToken = userDefaults.valueForKey("AuthorizationToken") {
            // do something here when a OAuthToken exists
            Router.OAuthToken = OAuthToken as? String

        }
        else {
            // no OAuthToken exists
        }
        print("AuthorizationToken: \(Router.OAuthToken)")
                print("dish ID: \(dish?.dish_id)")
                print("User Rating : \(self.userRatingView.rating)")

        UIApplication.sharedApplication().networkActivityIndicatorVisible = true

        Alamofire.request(Router.rateDish((dish?.dish_id)!, String(format:"%.2f", self.userRatingView.rating))).responseJSON { result in
            print(result)
            debugPrint(result)
            print("Parsed JSON: \(result.result.value)")
            print("JSON Error: \(result.result.error)")

            print("JSON Error2: \(result.result.value!["errors"])")
            print("JSON Error2: \(result.result.value!["data"])")

            let y: AnyObject = (result.result.value?["data"])!
            if let _ = y as? NSDictionary {
                print("Yes")

            }
            else{
                print("No")

                //display error alert.
                if let errorMsg: AnyObject = (result.result.value?["errors"])!{

                    let alert = UIAlertController(title: "Error", message:errorMsg as? String , preferredStyle: UIAlertControllerStyle.Alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                    alert.addAction(defaultAction)
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                
            }

            UIApplication.sharedApplication().networkActivityIndicatorVisible = false

        }

    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
