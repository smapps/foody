//
//  SignupViewController.swift
//  Foody
//
//  Created by Sameh Mabrouk on 11/21/15.
//  Copyright © 2015 smapps. All rights reserved.
//
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import UIKit
import Alamofire

struct MoveKeyboard {
    static let KEYBOARD_ANIMATION_DURATION : CGFloat = 0.3
    static let MINIMUM_SCROLL_FRACTION : CGFloat = 0.2;
    static let MAXIMUM_SCROLL_FRACTION : CGFloat = 0.8;
    static let PORTRAIT_KEYBOARD_HEIGHT : CGFloat = 216;
    static let LANDSCAPE_KEYBOARD_HEIGHT : CGFloat = 162;
}

class SignupViewController: UIViewController, ValidationDelegate {

    @IBOutlet var userNameTF : UITextField!
    @IBOutlet var invitationCodeTF : UITextField!
    @IBOutlet var passwordTF : UITextField!

    @IBOutlet weak var signUpButton: TKTransitionSubmitButton!

    let validator = Validator()

    var animateDistance: CGFloat!

    override func viewDidLoad() {
        super.viewDidLoad()


        // Do any additional setup after loading the view.

      //  self.navigationController?.navigationBarHidden = true
        
        //Round textfields.
        userNameTF.layer.borderWidth = 1
        userNameTF.layer.cornerRadius = 15
        userNameTF.clipsToBounds = true

        invitationCodeTF.layer.borderWidth = 1
        invitationCodeTF.layer.cornerRadius = 15
        invitationCodeTF.clipsToBounds = true

        passwordTF.layer.borderWidth = 1
        passwordTF.layer.cornerRadius = 15
        passwordTF.clipsToBounds = true

        let btnName = UIButton()
        btnName.setImage(UIImage(named: "back"), forState: .Normal)
        btnName.frame = CGRectMake(0, 0, 28, 28)
        btnName.addTarget(self, action: Selector("backAction"), forControlEvents: .TouchUpInside)

        //.... Set Right/Left Bar Button item
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.leftBarButtonItem = rightBarButton



        //Validator Work
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "hideKeyboard"))

        validator.styleTransformers(success:{ (validationRule) -> Void in
            print("here")
            // clear error label
//            validationRule.errorLabel?.hidden = true
//            validationRule.errorLabel?.text = ""

                        validationRule.textField.layer.borderColor = UIColor.greenColor().CGColor
                        validationRule.textField.layer.borderWidth = 0.5

            }, error:{ (validationError) -> Void in
                print("error")
//                validationError.errorLabel?.hidden = false
//                validationError.errorLabel?.text = validationError.errorMessage
                                validationError.textField.layer.borderColor = UIColor.redColor().CGColor
                                validationError.textField.layer.borderWidth = 1.0
        })


        validator.registerField(userNameTF, errorLabel: UILabel(), rules: [RequiredRule(), EmailRule()])
        validator.registerField(passwordTF, errorLabel: UILabel(), rules: [RequiredRule()])
        validator.registerField(invitationCodeTF, errorLabel: UILabel(), rules: [RequiredRule()])

    }

    func backAction() {
        self.navigationController?.popViewControllerAnimated(true)
    }

    func didStartYourLoading() {
        signUpButton.startLoadingAnimation()
    }

    func didFinishYourLoading() {
        signUpButton.startFinishAnimation(1, completion: { () -> () in
            //Your Transition
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewControllerWithIdentifier("LoginVC") as! MainFeedTableViewController
            self.navigationController?.pushViewController(vc, animated: true)
        })
    }

    // MARK: Action Methods
    @IBAction func submitTapped(sender: AnyObject) {
        print("Validating...")
        validator.validate(self)
    }

    @IBAction func onTapButton(button: TKTransitionSubmitButton) {
        button.animate(1, completion: { () -> () in

        })
    }

    // MARK: ValidationDelegate Methods

    func validationSuccessful() {
        print("Validation Success!")
        /*
        let alert = UIAlertController(title: "Success", message: "You are validated!", preferredStyle: UIAlertControllerStyle.Alert)
        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        self.presentViewController(alert, animated: true, completion: nil)
        */
        self.didStartYourLoading()

        //call signup API.
        Alamofire.request(Router.ActivateAccount(self.userNameTF.text!, self.invitationCodeTF.text!, self.passwordTF.text!)).responseJSON { result in

            print(result)
            debugPrint(result)
            print("Parsed JSON: \(result.result.value)")
            print("JSON Error: \(result.result.error)")

            print("JSON Error2: \(result.result.value!["errors"])")
            print("JSON Error2: \(result.result.value!["data"])")

            let y: AnyObject = (result.result.value?["data"])!
            if let _ = y as? NSDictionary {
                print("Yes")
                //finsh loading and redirect to Welcome screen.
                self.didFinishYourLoading()
            }
            else{
                print("No")
                self.signUpButton.startFinishAnimation(0, completion: { () -> () in

                    //display error alert.
                    if let errorMsg: AnyObject = (result.result.value?["errors"])!{

                        let alert = UIAlertController(title: "Error", message:errorMsg as? String , preferredStyle: UIAlertControllerStyle.Alert)
                        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                        alert.addAction(defaultAction)
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                    
                    
                    
                })

            }

        }


    }
    func validationFailed(errors:[UITextField:ValidationError]) {
        print("Validation FAILED!")
    }

    func hideKeyboard(){
        self.view.endEditing(true)
    }


    /*
    func textFieldDidBeginEditing(textField: UITextField) {
    animateViewMoving(true, moveValue: 100)
    }
    func textFieldDidEndEditing(textField: UITextField) {
    print("down")
    animateViewMoving(false, moveValue: 100)
    }

    func animateViewMoving (up:Bool, moveValue :CGFloat){
    let movementDuration:NSTimeInterval = 0.3
    let movement:CGFloat = ( up ? -moveValue : moveValue)
    UIView.beginAnimations( "animateView", context: nil)
    UIView.setAnimationBeginsFromCurrentState(true)
    UIView.setAnimationDuration(movementDuration )
    self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
    UIView.commitAnimations()
    }

    */

    //Moving Textfields when Keyboard appears.

    // MARK: UITextFieldDelegate Methods

    func textFieldDidBeginEditing(textField: UITextField) {
        let textFieldRect : CGRect = self.view.window!.convertRect(textField.bounds, fromView: textField)
        let viewRect : CGRect = self.view.window!.convertRect(self.view.bounds, fromView: self.view)

        let midline : CGFloat = textFieldRect.origin.y + 0.5 * textFieldRect.size.height
        let numerator : CGFloat = midline - viewRect.origin.y - MoveKeyboard.MINIMUM_SCROLL_FRACTION * viewRect.size.height
        let denominator : CGFloat = (MoveKeyboard.MAXIMUM_SCROLL_FRACTION - MoveKeyboard.MINIMUM_SCROLL_FRACTION) * viewRect.size.height
        var heightFraction : CGFloat = numerator / denominator

        if heightFraction < 0.0 {
            heightFraction = 0.0
        } else if heightFraction > 1.0 {
            heightFraction = 1.0
        }

        let orientation : UIInterfaceOrientation = UIApplication.sharedApplication().statusBarOrientation
        if (orientation == UIInterfaceOrientation.Portrait || orientation == UIInterfaceOrientation.PortraitUpsideDown) {
            animateDistance = floor(MoveKeyboard.PORTRAIT_KEYBOARD_HEIGHT * heightFraction)
        } else {
            animateDistance = floor(MoveKeyboard.LANDSCAPE_KEYBOARD_HEIGHT * heightFraction)
        }

        var viewFrame : CGRect = self.view.frame
        viewFrame.origin.y -= animateDistance

        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(NSTimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))

        self.view.frame = viewFrame

        UIView.commitAnimations()
    }


    func textFieldDidEndEditing(textField: UITextField) {
        var viewFrame : CGRect = self.view.frame
        viewFrame.origin.y += animateDistance

        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)

        UIView.setAnimationDuration(NSTimeInterval(MoveKeyboard.KEYBOARD_ANIMATION_DURATION))

        self.view.frame = viewFrame

        UIView.commitAnimations()

        //
        print("textFieldDidEndEditing")
        //        Networking.checkUserNameAvailability("smapps")

    }

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
