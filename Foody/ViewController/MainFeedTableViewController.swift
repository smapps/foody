//
//  MainFeedTableViewController.swift
//  Foody
//
//  Created by Sameh Mabrouk on 11/21/15.
//  Copyright © 2015 smapps. All rights reserved.
//
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import UIKit
import Alamofire
import ReachabilitySwift
import AlamofireImage

class MainFeedTableViewController: UITableViewController, FloatRatingViewDelegate {

    var dataSource : Array<Dish> = []
    var ImageCache = [String:UIImage]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()

        let btnName = UIButton()
        btnName.setImage(UIImage(named: "profile"), forState: .Normal)
        btnName.frame = CGRectMake(0, 0, 20, 20)
        btnName.addTarget(self, action: Selector("gotoProfileVC"), forControlEvents: .TouchUpInside)

        //.... Set Right/Left Bar Button item
        let rightBarButton = UIBarButtonItem()
        rightBarButton.customView = btnName
        self.navigationItem.leftBarButtonItem = rightBarButton

        // Add a refresh control, pull down to refresh.
        self.refreshControl = UIRefreshControl()
        self.refreshControl!.addTarget(self, action: "retrieveListOfDishes", forControlEvents: UIControlEvents.ValueChanged)

        self.beginRefreshingTableData()
    }

    override func viewWillAppear(animated: Bool) {

        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.barTintColor = UIColor.foodyOrangeColor()
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
    }


    // MARK: - Content Refresh

    func beginRefreshingTableData() {

        // Let the show begins by adding the refresh control to UItableView.
        var tableViewCenter:CGPoint = self.tableView.contentOffset
        tableViewCenter.y += self.tableView.frame.size.height/2
        self.tableView.setContentOffset(CGPointMake(0, -190), animated: true)

        self.refreshControl?.beginRefreshing()

        self.retrieveListOfDishes()
        
    }

    // MARK: - List Dishes

    func retrieveListOfDishes() {
        //call List Dishes API.
        let userDefaults = NSUserDefaults.standardUserDefaults()
        if let OAuthToken = userDefaults.valueForKey("AuthorizationToken") {
            // do something here when a OAuthToken exists
            Router.OAuthToken = OAuthToken as? String

        }
        else {
            // no OAuthToken exists
        }
        print("AuthorizationToken: \(Router.OAuthToken)")

        Alamofire.request(Router.ListDishes).responseJSON { result in
            print(result)
            debugPrint(result)
            print("Parsed JSON: \(result.result.value)")
            print("JSON Error: \(result.result.error)")

            print("JSON Error2: \(result.result.value!["errors"])")
            print("JSON Error2: \(result.result.value!["data"])")

            let y: AnyObject = (result.result.value?["data"])!
            if let _ = y as? NSDictionary {
                print("Yes")

                //update tableview with data.
                self.dataSource = self.dishItems(result)
                print("dataSource count", self.dataSource.count)
                self.tableView.reloadData()

            }
            else{
                print("No")

                    //display error alert.
                    if let errorMsg: AnyObject = (result.result.value?["errors"])!{

                        let alert = UIAlertController(title: "Error", message:errorMsg as? String , preferredStyle: UIAlertControllerStyle.Alert)
                        let defaultAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                        alert.addAction(defaultAction)
                        self.presentViewController(alert, animated: true, completion: nil)
                    }

            }

            self.refreshControl?.endRefreshing()

        }

    }

    //Extract dish item from JSON, then retuen array with all dishes.
    func dishItems(json: Response<AnyObject,NSError>) -> (Array<Dish>) {

        print("dishItems")
        var refinedDeshItems : Array<Dish> = []

        let y: AnyObject = (json.result.value?["data"])!
        if let dataDict = y as? NSDictionary {

            //get dishes from json
            if let rawDishItems = dataDict["dishes"] as? [AnyObject] {

                // convert the NSDictinary Foundation object into individual data object
                // and place these objects into a single response array that is returned.
                for itemDict in rawDishItems{

                    var deshId:String?
                    var deshName:String?
                    var deshDesc:String?
                    var deshDeliveryDate:String?
                    var deshImageURL:String?
                    var deshUserRating:String?
                    var deshAverageRating:String?
                    var deshIsLiked:String?

                    var restaurant:Restaurant?

                    print("desh ID", itemDict["id"] as? Int)

//                    deshId = itemDict["id"] as? String

                    deshId = String(format:"%d", (itemDict["id"] as? Int)!)

                    deshImageURL = itemDict["image_path"] as? String
                    deshUserRating = itemDict["user_rating"] as? String
                    deshAverageRating = itemDict["average_rating"] as? String
                    deshIsLiked = itemDict["is_liked"] as? String

                    if let name = (itemDict["name"] as? String) {
                        print("name", name)
                        deshName = name

                    }
                    if let desc = (itemDict["description"] as? String) {
//                        print("Description", desc)
                        deshDesc = desc

                    }
                    if let lang = (itemDict["delivery_date"] as? String) {
//                        print("delivery date", lang)
                        deshDeliveryDate = lang

                    }
                    if let restDict = (itemDict["restaurant"] as? NSDictionary) {
                        print("restaurant obj")

                        restaurant = Restaurant(rest_id: restDict["id"] as? String, rest_name: restDict["name"] as? String, rest_desc: restDict["description"] as? String, rest_LogoPath: restDict["logo_path"] as? String)

                    }
                    print("image_path", deshImageURL)


                    let repo : Dish = Dish(dish_id: deshId, dish_name: deshName, dish_imageUrl: deshImageURL, dish_description: deshDesc, dish_userRating: deshUserRating, dish_averageRating: deshAverageRating, dish_isLiked: deshIsLiked, dish_deliveryDate: deshDeliveryDate,restaurant: restaurant)
                    refinedDeshItems.append(repo)

                }
            }


        }

        return refinedDeshItems

    }

    func gotoProfileVC() {

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("ProfileVC") as! ProfileViewController
        self.navigationController?.pushViewController(vc, animated: true)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return self.dataSource.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }


    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MainFeedCellID", forIndexPath: indexPath)

        // Configure the cell...
        let deshItem : Dish = self.dataSource[indexPath.section]
        let repoTitleLabel:UILabel = cell.viewWithTag(100) as! UILabel
        repoTitleLabel.text = deshItem.dish_name

        let dishDeliveryDateLabel:UILabel = cell.viewWithTag(101) as! UILabel
        dishDeliveryDateLabel.text = deshItem.dish_deliveryDate

        let dishDescLabel:UILabel = cell.viewWithTag(102) as! UILabel
        dishDescLabel.text = deshItem.dish_description

        //Setup Rating Control.

        /** Note: With the exception of contentMode, all of these
        properties can be set directly in Interface builder **/

        let ratingView:FloatRatingView = cell.viewWithTag(105) as! FloatRatingView

        // Required float rating view params
        ratingView.emptyImage = UIImage(named: "gray-star")
        ratingView.fullImage = UIImage(named: "gold-star")
        // Optional params
        ratingView.delegate = self
        ratingView.contentMode = UIViewContentMode.ScaleAspectFit
        ratingView.maxRating = 5
        ratingView.minRating = 1
        ratingView.rating = (deshItem.dish_userRating! as NSString).floatValue
        ratingView.editable = false
        ratingView.halfRatings = false
        ratingView.floatRatings = false

//        let dishImageView:UIImageView = cell.viewWithTag(104) as! UIImageView
//        let URL = NSURL(string:deshItem.dish_imageUrl!)!
//        dishImageView.af_setImageWithURL(URL)

        let dishName = deshItem.dish_name


        //This is a workaround to cash the image and improve the performance while user scrolling UITableView.
        //Note: the image url returned from API is loading different image every time you call it.

        print("commit from cach ", ImageCache[dishName!])
        // If this image is already cached, don't re-download
        if let dishImage = ImageCache[dishName!] {
                    let dishImageView:UIImageView = cell.viewWithTag(104) as! UIImageView
                    dishImageView.image = dishImage
        }
        else {
            //Download image
            // We should perform this in a background thread
            Alamofire.request(.GET, deshItem.dish_imageUrl!)
                .responseImage { response in
                    debugPrint(response)

                    print(response.request)
                    print(response.response)
                    debugPrint(response.result)

                    if let image = response.result.value {
                        print("image downloaded: \(image)")

                        // Store the commit date in to our cache
                        self.ImageCache[dishName!] = image

                        // Update the cell
                        dispatch_async(dispatch_get_main_queue(), {
                            if let cellToUpdate = tableView.cellForRowAtIndexPath(indexPath) {
                                    let dishImageView:UIImageView = cellToUpdate.viewWithTag(104) as! UIImageView
                                    dishImageView.image = image

                            }
                        })

                    }
            }

        }



        return cell
    }

    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.5; // space b/w cells
    }

    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        header.backgroundColor = UIColor.clearColor()
        return header
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        print("didDeselectRowAtIndexPath")
        //Transition
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewControllerWithIdentifier("MealDetailsVCID") as! MealDetailsViewController
        vc.dish = self.dataSource[indexPath.section]
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func ratingTypeChanged(sender: UISegmentedControl) {
    }

    // MARK: FloatRatingViewDelegate

    func floatRatingView(ratingView: FloatRatingView, isUpdating rating:Float) {
    }

    func floatRatingView(ratingView: FloatRatingView, didUpdate rating: Float) {

    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return false if you do not want the specified item to be editable.
    return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return false if you do not want the item to be re-orderable.
    return true
    }
    */
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
