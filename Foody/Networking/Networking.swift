//
//  Networking.swift
//  Fennecy
//
//  Created by Sameh Mabrouk on 9/20/15.
//  Copyright © 2015 smapps. All rights reserved.
//
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import Foundation
import Alamofire

@objc public protocol ResponseObjectSerializable {
    init(response: NSHTTPURLResponse, representation: AnyObject)
}
extension Alamofire.Request {

}


/**
 * Response Object Collection Extension
 */

@objc public protocol ResponseCollectionSerializable {
    //    static func collection(response response: NSHTTPURLResponse, representation: AnyObject) -> [Self]
}


extension Alamofire.Request {
}

/**
 * Our Networking class
 */

final class Networking {

    // Get nearby events by a provided Zip Code
    class func getEventsNearby() {
    }

    class func checkUserNameAvailability(userName:String) {
        Alamofire.request(.GET, "https://fbackendnew.azurewebsites.net/api/Users", parameters:  ["Username": userName])
            .responseJSON { result in
                print(result)
                debugPrint(result)

                print("Parsed JSON: \(result.result.value)")
        }
    }


    class func signUpNewUser() {
        Alamofire.request(.POST, "https://fbackendnew.azurewebsites.net/api/Users", parameters:  ["Email": "mabrouksameh@gmail.com", "Username": "smapps", "Password": "123", "Client_Id": "iOS", "Client_Secret": "42d83b0e-02d1-4cba-894c-e337f7efe11e"])
            .responseJSON { result in
                print(result)
                debugPrint(result)

                print("Parsed JSON: \(result.result.value)")
        }
    }
}

enum Router: URLRequestConvertible{
//http://foody.jumpsuite.net/api/v1/auth/register
    static let baseURLString = "http://188.226.251.69/api/v1"
//    static let baseURLString = "http://foody.jumpsuite.net/api/v1"
    static var OAuthToken: String?
    static var PublicToken: String?

    static let clientId = "iOS"
    static let clientSecret = "42d83b0e-02d1-4cba-894c-e337f7efe11e"

    case NewUser(String, String, String, String)
    case ActivateAccount(String, String, String)
    case loginUser(String, String)
    case forgotPassword(String)
    case ListDishes
    case rateDish(String, String)
    case showInterest(String, String)
    case updateUserData(String, String, String, String)
    case getUserdata




    var method: Alamofire.Method {
        switch self {
        case .NewUser, .rateDish, .showInterest, .updateUserData:
            return .POST
        case .ActivateAccount:
            return .POST
        case .loginUser:
            return .POST
        case .forgotPassword:
            return .POST
        case .ListDishes, .getUserdata:
            return .GET
        }

    }

    var path: String {
        switch self {
        case .NewUser:
            return "/auth/register"
        case .ActivateAccount:
            return "/auth/activate"
        case .loginUser:
            return "/auth/login"
        case .forgotPassword:
            return "/auth/reset-password"
        case .ListDishes:
            return "/dishes"
        case .rateDish:
            return "/dishes/rate"
        case .showInterest:
            return "/dishes/show-interest"
        case .updateUserData:
            return "/users/user"
        case .getUserdata:
            return "/users/user"
        }
    }

    var parameters: [String: AnyObject] {
        switch self {
        case .NewUser (let userName, let email, let phone, let address):
            let params = ["name": userName, "email": "\(email)", "phone": phone, "address": address]
            return params

        case .ActivateAccount (let email, let invitationCode, let newPass):
            let params = ["email": email, "invitation_code": "\(invitationCode)", "new_password": newPass]
            return params

        case .loginUser(let email, let Password):
            let params = ["email": email, "password": "\(Password)"]
            return params

        case .forgotPassword(let email):
            let params = ["email": email]
            return params

        case .rateDish(let dishId, let rating):
             let params = ["dish_id": dishId, "rating": "\(rating)"]
            return params

        case .showInterest(let dishId, let is_liked):
            let params = ["dish_id": dishId, "is_liked": "\(is_liked)"]
            return params

        case .updateUserData(let name, let password, let phone, let address):
            let params = ["name": name, "password": "\(password)", "phone": phone, "address": "\(address)"]
            return params

        default:
            return self.parameters
        }
    }


    // MARK: URLRequestConvertible
    var URLRequest: NSMutableURLRequest {
        /*
        let path: String, parameters: [String: AnyObject] = {

        switch self {
        case .NewUser (let userName, let email, let password):
        let params = ["Username": userName, "Email": "\(email)", "Password": password, "Client_Id": Router.clientId,  "Client_Secret": Router.clientSecret]
        return params
        }

        }()
        */

        Router.PublicToken = "CeF4iM18GO"

        let URL = NSURL(string: Router.baseURLString)!
        //        let URLRequest = NSURLRequest(URL: URL!.URLByAppendingPathComponent(path))
        let mutableURLRequest = NSMutableURLRequest(URL: URL.URLByAppendingPathComponent(path))
        mutableURLRequest.HTTPMethod = method.rawValue

        //add public token to headers
        if let token = Router.PublicToken {
            mutableURLRequest.setValue(token, forHTTPHeaderField: "PUBLIC-TOKEN")
        }

        //add OAuthToken to headers
        if let token = Router.OAuthToken {
            mutableURLRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        }


        switch self {

        case .NewUser, .ActivateAccount, .loginUser, .forgotPassword, .rateDish, .showInterest, .updateUserData:
            return Alamofire.ParameterEncoding.JSON.encode(mutableURLRequest, parameters: parameters).0

        default:
            return mutableURLRequest
        }

    }
    
}
